#!/bin/bash

setOsToMacOSX() {
  abOsType=macosx
  source ${libFolder}/${abOsType}.bash
}

setOsToDebian() {
  abOsType=debian
  source ${libFolder}/${abOsType}.bash
}
