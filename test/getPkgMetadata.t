#!/bin/bash

thisFolder=$(dirname $0)

source ${thisFolder}/../lib/common.bash
source ${thisFolder}/testlib.bash

testDebianSpotifyPackageIsNotMacApp() {
  setOsToDebian
  assertEquals '' "`getPkgMetadata spotify isMacApp`"
}

testMacOSXSpotifyPackageIsMacApp() {
  setOsToMacOSX
  assertEquals 'isMacApp' "`getPkgMetadata spotify isMacApp`"
}

. shunit2
