#!/bin/bash

thisFolder=$(dirname $0)

source ${thisFolder}/../lib/common.bash
source ${thisFolder}/testlib.bash

testDebianSpotifyPackageTranslates2SpotifyClient() {
  setOsToDebian
  assertEquals 'spotify-client' `searchOsPkgNameOrDefault spotify`
}

testMacOSXSpotifyPackageTranslates2SpotifyClient() {
  setOsToMacOSX
  assertEquals 'spotify' `searchOsPkgNameOrDefault spotify`
}

. shunit2
