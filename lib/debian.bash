#!/usr/bin/env bash

export bashInitFile=.bashrc

function pkgInstall() {
    execPkgPreInstall ${1}
    sudo apt-get install `searchOsPkgNameOrDefault $1`
    execPkgPostInstall ${1}
}
