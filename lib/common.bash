#!/usr/bin/env bash

thisProgram=$(basename $0)
libFolder=$(dirname ${BASH_SOURCE[0]})
abOsType='?'

## OS based functionality load
case $OSTYPE in
  darwin*)
      abOsType=macosx
    ;;
  linux*)
      abOsType=debian
    ;;
esac

source ${libFolder}/${abOsType}.bash
## END - OS based functionality load

function showMsg() {
    echo "$thisProgram: $1"
}

function showErr() {
    showMsg "ERROR: $*"
}

function exitNoInstallMethod() {
    showErr “No install method for this platform”
    exit -1
}

function installAndDownloadZip() {
    local fileUrl=${1}
    local destFolder=${2}
    local filePath=/tmp/$$.zip
    curl -L -o ${filePath} ${fileUrl} && \
       unzip -d ${destFolder} ${filePath}
}

function installAndDownloadPackage() {
    local pkgUrl=${1}
    local pkgPath=/tmp/${2}
    curl -L -o ${pkgPath} $pkgUrl  && sudo apt install ${pkgPath}
}

function installPkgDependencies() {
    sudo apt install $*
}

function installScriptFromUrl() {
    curl -sL $1 | sudo -E bash -
}

function searchOsPkgNameOrDefault() {
  local pkg=`grep '^'$1'|' ${libFolder}/${abOsType}-pkg.lst| awk -F\| '{print $2}'`
  [[ -z $pkg ]] && echo $1 && return
  echo $pkg
}

function execPkgPreInstall() {
  local pkg=${1}
  local file=`grep '^'${pkg}'|' ${libFolder}/${abOsType}-pkg.lst| awk -F\| '{print $3}'`
  [[ ! -z $file ]] && ${libFolder}/${file}
}

function execPkgPostInstall() {
  local pkg=${1}
  local file=`grep '^'${pkg}'|' ${libFolder}/${abOsType}-pkg.lst| awk -F\| '{print $4}'`
  [[ ! -z $file ]] && ${libFolder}/${file}
}

function getPkgMetadata() {
  local pkg=${1}
  local metadata=`grep '^'${pkg}'|' ${libFolder}/${abOsType}-pkg.lst| awk -F\| '{print $5}'`
  FS='@' read -ra MD <<< "$metadata"
  for i in "${MD[@]}"; do
        [[ $i == ${2} ]] && echo $i && return
  done
  echo ""
}
