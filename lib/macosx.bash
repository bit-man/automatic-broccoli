#!/usr/bin/env bash

export bashInitFile=.bash_profile

function __initBrew() {
  local brewNotFound=`type brew 2>&1 | grep --count "found"`
  [[ $brewNotFound -eq 1 ]] && \
    showMsg "Installing Homebrew package manager" && \
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

  local gitNotFound=`type git 2>&1 | grep --count "found"`
  [[ $gitNotFound -eq 1 ]] && brew install git
}

function pkgInstall() {
  __initBrew
  execPkgPreInstall ${1}

  local prefix=""
  [[ ! -z `getPkgMetadata $1 isMacApp` ]] && prefix="cask"
  brew  install ${prefix} `searchOsPkgNameOrDefault $1`

  execPkgPostInstall ${1}
}
